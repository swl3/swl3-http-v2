package ru.swayfarer.swl3.httpv2.module;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.httpv2.IUrlEncodedParams;
import ru.swayfarer.swl3.httpv2.UrlEncodedParams;
import ru.swayfarer.swl3.httpv2.serialization.BodySerializerFinder;
import ru.swayfarer.swl3.httpv2.serialization.FindBodySerializerEvent.SerializationStage;
import ru.swayfarer.swl3.httpv2.serialization.MultipartBodySerializer;
import ru.swayfarer.swl3.httpv2.serialization.PlainTextSerializer;
import ru.swayfarer.swl3.httpv2.serialization.RawStreamSerializer;
import ru.swayfarer.swl3.httpv2.serialization.UrlFormEncodedSerializer;
import ru.swayfarer.swl3.io.streams.DataInStream;

import java.io.InputStream;
import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class DefaultHttpModule extends HttpV2Module
{
    @Override
    public void enrichBodySerializerFinder(BodySerializerFinder serializerFinder)
    {
        // The text/plain default content serializer for all string bodies
        serializerFinder.registerSerializer(new PlainTextSerializer(), -1,
            (cfg) ->
            {
                cfg
                    .addType(CharSequence.class)
                    .addType(String.class)
                    .addContentTypeInclusion("mask:*");
            }
        );

        // The raw stream content deserializer
        serializerFinder.registerSerializer(new RawStreamSerializer(), -1,
            (cfg) ->
            {
                cfg
                    .addCustomFilter((evt) -> InputStream.class.isAssignableFrom(evt.getType().getJavaType()))
                    .addContentTypeInclusion("mask:*");
            }
        );

        // The text/plain serializer for "text/plain" content-type
        serializerFinder.registerSerializer(new PlainTextSerializer(), 0,
            (cfg) ->
            {
                cfg
                    .addType(CharSequence.class)
                    .addType(String.class)
                    .addContentTypeInclusion("text/plain");
            }
        );

        // The ufl-form-encoded serializer for "application/x-www-form-urlencoded" content-type
        serializerFinder.registerSerializer(new UrlFormEncodedSerializer(), 0,
            (cfg) ->
            {
                cfg
                    .addType(Map.class)
                    .addType(IUrlEncodedParams.class)
                    .addType(UrlEncodedParams.class)
                    .addContentTypeInclusion("application/x-www-form-urlencoded");
            }
        );

        // The multipart file serializer
        serializerFinder.registerSerializer(new MultipartBodySerializer(), 0,
            (cfg) ->
            {
                cfg
                    .setStages(SerializationStage.Serialize)
                    .addType(InputStream.class)
                    .addType(DataInStream.class)
                    .addContentTypeInclusion("multipart/form-data");
            }
        );
    }
}
