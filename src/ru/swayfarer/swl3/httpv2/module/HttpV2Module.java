package ru.swayfarer.swl3.httpv2.module;

import ru.swayfarer.swl3.httpv2.client.factory.ClientSettingsCreationEvent;
import ru.swayfarer.swl3.httpv2.serialization.BodySerializerFinder;
import ru.swayfarer.swl3.markers.Internal;

public abstract class HttpV2Module
{
    public void enrichClientSettings(ClientSettingsCreationEvent<?> settingsCreationEvent) {}
    public void enrichBodySerializerFinder(BodySerializerFinder serializerFinder) {}

    @Internal
    public void startEnrich(ClientSettingsCreationEvent<?> settingsCreationEvent)
    {
        enrichClientSettings(settingsCreationEvent);
        enrichBodySerializerFinder(settingsCreationEvent.getResult().getBodySerializerFinder());
    }
}
