package ru.swayfarer.swl3.httpv2.serialization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

/**
 * The event that represents {@link IBodySerializer} search at {@link BodySerializerFinder}
 * @see BodySerializerFinder#eventFindSerializer
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FindBodySerializerEvent extends AbstractEvent
{
    /**
     * The http content-type value
     */
    @Internal
    @NonNull
    public String httpContentType;

    /**
     * The java type representation
     */
    @Internal
    @NonNull
    public SerializationType type;

    /**
     * The result that found while event processing by any subscribers <br>
     * If not null than any subscribers will be canceled for that event.
     */
    @Internal
    public IBodySerializer result;

    @Internal
    public SerializationStage stage;

    public static enum SerializationStage {
        Serialize, Deserialize
    }
}
