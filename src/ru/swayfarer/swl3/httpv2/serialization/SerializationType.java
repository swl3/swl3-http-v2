package ru.swayfarer.swl3.httpv2.serialization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.funs.ExceptionOptional;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.reflection.generic.GenericObject;

import java.lang.reflect.Type;

@Getter
@Setter
@Accessors(chain = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SerializationType
{
    @NonNull
    public Class<?> javaType;

    public Type genericType;

    public GenericObject genericObject;

    public boolean hasGenericObject()
    {
        return genericObject != null;
    }

    public ExtendedOptional<GenericObject> getGenericOptional()
    {
        return ExtendedOptional.of(genericObject);
    }

    @Override
    public String toString()
    {
        if (genericObject != null)
            return genericObject.toGenericString();

        else if (genericType != null)
            return genericType.getTypeName();

        else if (javaType != null)
            return javaType.getName();

        return super.toString();
    }
}
