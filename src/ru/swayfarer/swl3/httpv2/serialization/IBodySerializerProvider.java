package ru.swayfarer.swl3.httpv2.serialization;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Alias;

/**
 * The alias for {@link IFunction1NoR} with {@link FindBodySerializerEvent} param
 * for more fancy java code. Syntax sugar.
 */
@Alias("IFunction1NoR<FindBodySerializerEvent>")
public interface IBodySerializerProvider extends IFunction1NoR<FindBodySerializerEvent>
{
    @Override
    public default void applyNoRUnsafe(FindBodySerializerEvent findBodySerializerEvent) throws Throwable
    {
        provide(findBodySerializerEvent);
    }

    /**
     * Provide body serializer for requested event.
     * @param event Requested event
     * @throws Throwable if any exception occurs
     */
    public void provide(FindBodySerializerEvent event) throws Throwable;
}
