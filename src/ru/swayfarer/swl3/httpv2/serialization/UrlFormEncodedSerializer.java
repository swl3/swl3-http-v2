package ru.swayfarer.swl3.httpv2.serialization;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.httpv2.IUrlEncodedParams;
import ru.swayfarer.swl3.httpv2.UrlEncodedParams;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.converters.StringConverters;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class UrlFormEncodedSerializer implements IBodySerializer
{
    @Internal
    @NonNull
    public StringConverters stringConverters;

    public UrlFormEncodedSerializer()
    {
        this(StringConverters.getInstance());
    }

    public UrlFormEncodedSerializer(@NonNull StringConverters stringConverters)
    {
        this.stringConverters = stringConverters;
    }

    @Override
    public Object deserialize(DataInStream dis, SerializationType type, IHttpResponse response)
    {
        var javaType = type.getJavaType();

        if (javaType != null && IUrlEncodedParams.class.isAssignableFrom(javaType))
        {
            var result = new UrlEncodedParams(stringConverters);
            var paramsString = dis.readAllAsString();

            var paramsEntries = paramsString.split("&");

            for (var paramEntry : paramsEntries)
            {
                if (paramEntry.contains("=[]"))
                    paramEntry = paramEntry.replace("=[]", "=");

                var paramEntryParts = paramEntry.split("=");
                ExceptionsUtils.IfNot(paramEntryParts.length == 2, IllegalArgumentException.class, "Illegal url-form-encoded param entry:", paramEntry, "it must be a key=value record! (of form string", paramsString, ")");

                result.add(StringUtils.strip(paramEntryParts[0]), StringUtils.strip(paramEntryParts[1]));
            }

            return result;
        }
        else if (Map.class.equals(javaType))
        {
            var generics = type.getGenericOptional().orElseThrow(() -> new IllegalStateException("Can't deserialize map from url-form-encoded without generics!"));

            // Check that generics are valid.
            ExceptionsUtils.IfNot(generics.getChilds().size() == 2, IllegalArgumentException.class, "For map deserialization you must provide two generics: <Key, Value>, not a", generics.getChilds().size());
            ExceptionsUtils.IfNot(generics.getChilds().get(0).getChilds().isEmpty(), IllegalArgumentException.class, "Map key parameter type for deserialization must not has any own generics! Not a", generics.getChilds().get(0).toGenericString());
            ExceptionsUtils.IfNot(generics.getChilds().get(1).getChilds().isEmpty(), IllegalArgumentException.class, "Map value parameter type for deserialization must not has any own generics! Not a", generics.getChilds().get(1).toGenericString());

            var keyType = generics.getChilds().get(0).loadClass();
            var valueType = generics.getChilds().get(1).loadClass();

            var paramsString = dis.readAllAsString();
            var paramsEntries = paramsString.split("&");
            var allAsArrayMap = new ExtendedMap<Object, List<Object>>();

            for (var paramEntry : paramsEntries)
            {
                if (paramEntry.contains("=[]"))
                    paramEntry = paramEntry.replace("=[]", "=");

                var paramEntryParts = paramEntry.split("=");
                ExceptionsUtils.IfNot(paramEntryParts.length == 2, IllegalArgumentException.class, "Illegal url-form-encoded param entry:", paramEntry, "it must be a key=value record! (of form string", paramsString, ")");

                var key = stringConverters.convert(keyType, paramEntryParts[0]);
                var value = stringConverters.convert(valueType, paramEntryParts[1]);

                ExceptionsUtils.IfNull(key, IllegalArgumentException.class, "Can't convert url-form-encoded entry", paramEntry, "key", paramEntryParts[0], "to", keyType, "(of form string", paramsString, ")");
                ExceptionsUtils.IfNull(value, IllegalArgumentException.class, "Can't convert url-form-encoded entry", paramEntry, "value", paramEntryParts[1], "to", keyType, "(of form string", paramsString, ")");

                allAsArrayMap.getOrCreate(key, ExtendedList::new).add(value);
            }

            return removeSingletonArrays(allAsArrayMap);
        }

        ExceptionsUtils.throwException(new IllegalArgumentException("Can't read type " + javaType + " from url-form-encoded! Please, use the" + IUrlEncodedParams.class));
        return null;
    }

    public ExtendedMap<Object, Object> removeSingletonArrays(ExtendedMap<Object, List<Object>> map)
    {
        if (map == null)
            return null;

        var result = new ExtendedMap<>();

        for (var entry : map)
        {
            var key = entry.getKey();
            var value = entry.getValue();

            if (value.size() == 1)
                result.put(key, value.get(0));
            else
                result.put(key, value);
        }

        return result;
    }

    @Override
    public void serialize(Object instance, DataOutStream dos, SerializationType type, IWritableHttpRequest request)
    {
        var javaType = type.getJavaType();
        if (javaType != null && IUrlEncodedParams.class.isAssignableFrom(javaType))
        {
            var params = (IUrlEncodedParams) instance;
            var paramKeys = params.getKeys();
            var parts = new ExtendedList<String>();

            for (var paramKey : paramKeys)
            {
                var paramValues = params.getCollection(paramKey);
                if (paramValues.size() == 1)
                {
                    var buffer = new DynamicString();
                    buffer.append(paramKey);
                    buffer.append("=");
                    buffer.append(paramValues.first().getStringValue());
                    parts.add(buffer.toString());
                }
                else
                {
                    for (var paramValue : paramValues)
                    {
                        var buffer = new DynamicString();
                        buffer.append(paramKey);
                        buffer.append("[]=");
                        buffer.append(paramValue.getStringValue());
                        parts.add(buffer.toString());
                    }
                }

            }

            dos.writeString(StringUtils.concatWith("&", parts));
            dos.close();
        }
        else if (javaType != null && Map.class.isAssignableFrom(javaType))
        {
            var params = (Map<Object, Object>) instance;
            var parts = new ExtendedList<String>();

            for (var paramEntry : params.entrySet())
            {
                var key = String.valueOf(paramEntry.getKey());
                var valueObj = paramEntry.getValue();

                if (valueObj instanceof Iterable)
                {
                    for (var valueObjElem : (Iterable) valueObj)
                    {
                        var buffer = new DynamicString();
                        buffer.append(key);
                        buffer.append("[]=");
                        buffer.append(valueObjElem);
                        parts.add(buffer.toString());
                    }
                }
                else
                {
                    var buffer = new DynamicString();
                    buffer.append(key);
                    buffer.append("=");
                    buffer.append(valueObj);
                    parts.add(buffer.toString());
                }
            }

            dos.writeString(StringUtils.concatWith("&", parts));
            dos.close();
        }
        else
        {
            ExceptionsUtils.throwException(new IllegalArgumentException("Can't convert object " + instance + " to url-form-encoded! Please, use the" + IUrlEncodedParams.class));
        }
    }
}
