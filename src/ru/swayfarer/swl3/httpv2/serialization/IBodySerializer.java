package ru.swayfarer.swl3.httpv2.serialization;

import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;

public interface IBodySerializer
{
    public Object deserialize(DataInStream dis, SerializationType type, IHttpResponse response);
    public void serialize(Object instance, DataOutStream dos, SerializationType type, IWritableHttpRequest request);
}
