package ru.swayfarer.swl3.httpv2.serialization;

import lombok.var;
import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;

import java.io.InputStream;
import java.util.UUID;

public class MultipartBodySerializer implements IBodySerializer
{
    @Override
    public Object deserialize(DataInStream dis, SerializationType type, IHttpResponse response)
    {
        return dis;
    }

    @Override
    public void serialize(Object instance, DataOutStream dos, SerializationType type, IWritableHttpRequest request)
    {
        if (instance instanceof InputStream)
        {
            String crlf = "\r\n";
            String twoHyphens = "--";

            var boundary = "boundary_" + UUID.randomUUID().toString().replace("-", "");
            request.headers().setHeaders("Cache-Control", "no-cache");
            request.headers().setHeaders("Content-Type", "multipart/form-data;boundary=" + boundary);

            dos.writeString("Content-Disposition: form-data; name=\"" +
                    "fileName" + "\";filename=\"" +
                    "fileName" + "\"" + crlf);
            dos.writeString(crlf);

            dos.writeString(crlf);
            dos.writeString(twoHyphens + boundary + twoHyphens + crlf);

            StreamsUtils.copy()
                    .start((InputStream) instance, dos)
            ;

            dos.writeString(twoHyphens + boundary + crlf);

            dos.flush();
        }
    }
}
