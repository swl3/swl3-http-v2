package ru.swayfarer.swl3.httpv2.serialization;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.httpv2.serialization.FindBodySerializerEvent.SerializationStage;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.FilteringList;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Body serializer provider will search any
 * {@link IBodySerializer} for requested params, such {@link SerializationType} or {@link String}
 * that represents a http content-type
 * <br><br>
 * Contains observable field {@link #eventFindSerializer} which allows you to subscribe a custom
 * {@link IBodySerializerProvider} that contains functional for more flexible serializer find.
 */
@Getter
@Setter
@Accessors(chain = true)
public class BodySerializerFinder
{
    /**
     * The observable field that allows to subscribe any custom {@link IBodySerializerProvider}
     */
    @Internal
    @NonNull
    public IObservable<FindBodySerializerEvent> eventFindSerializer = Observables.<FindBodySerializerEvent>createObservable()
            // Process event only if result not present
            .execution().useFilter((e) -> e.getEventObject().getResult() == null)
            .exceptions().throwOnFail()
    ;

    /**
     * Register custom body serialization provider
     * @param provider Target provider
     * @param priority Provider priority at search providers queue
     * @return that instance (<code>this</code>). This is a chain method.
     */
    public BodySerializerFinder registerSerializerProvider(IBodySerializerProvider provider, int priority)
    {
        if (provider != null)
        {
            eventFindSerializer.subscribe().by(provider, priority);
        }

        return this;
    }

    /**
     * Register custom body serializer
     * @param serializer Target serializer
     * @param types Java types for which the body serializer can be applied
     * @param priority Provider priority at search providers queue
     * @return that instance (<code>this</code>). This is a chain method.
     */
    public BodySerializerFinder registerSerializer(IBodySerializer serializer, int priority, Class<?>... types)
    {
        return registerSerializer(serializer, priority, CollectionsSWL.list(types));
    }

    /**
     * Register custom body serializer
     * @param serializer Target serializer
     * @param types Java types for which the body serializer can be applied
     * @param priority Provider priority at search providers queue
     * @return that instance (<code>this</code>). This is a chain method.
     */
    public BodySerializerFinder registerSerializer(IBodySerializer serializer, int priority, Collection<Class<?>> types)
    {
        return registerSerializer(serializer, priority, (event) -> {
            return types.contains(event.getType().getJavaType());
        });
    }

    /**
     * Register custom body serializer
     * @param serializer Target serializer
     * @param filterCfgFun Function that allows to flexible configure filtering of request events
     * @param priority Provider priority at search providers queue
     * @return that instance (<code>this</code>). This is a chain method.
     */
    public BodySerializerFinder registerSerializer(IBodySerializer serializer, int priority, IFunction1NoR<FindSerializerEventFilter> filterCfgFun)
    {
        var filter = new FindSerializerEventFilter();

        if (filterCfgFun != null)
            filterCfgFun.apply(filter);

        return registerSerializer(serializer, priority, filter);
    }

    /**
     * Register custom body serializer
     * @param serializer Target serializer
     * @param filter Filter function that allows to control events for which the serializer can be applied
     * @param priority Provider priority at search providers queue
     * @return that instance (<code>this</code>). This is a chain method.
     */
    public BodySerializerFinder registerSerializer(IBodySerializer serializer, int priority, IFunction1<FindBodySerializerEvent, Boolean> filter)
    {
        if (serializer != null)
        {
            var provider = new AbstractBodySerializer();
            provider.setBodySerializerProvideFun((type) -> serializer);
            provider.setFilterFun(filter);
            return registerSerializerProvider(provider, priority);
        }

        return this;
    }

    /**
     * Find body serializer for current params
     * <h3>If body serializer will not be found that exception will be thrown!</h3>
     * @param type The java type representation of body object
     * @param contentType The http type (as header <code>Content-Type</code> value)
     * @throws RuntimeException If serializer not found
     * @return Result body serializer.
     */
    public IBodySerializer getSerializer(SerializationStage stage, SerializationType type, String contentType)
    {
        return getSerializer(stage, type, contentType, null);
    }

    /**
     * Find body serializer for current params
     * <h3>If body serializer will not be found that exception will be thrown!</h3>
     * @param type The java type representation of body object
     * @param contentType The http type (as header <code>Content-Type</code> value)
     * @param propertiesConfigurer The function which allows to configure serializer find event custom properties
     * @throws RuntimeException If serializer not found
     * @return Result body serializer.
     */
    public IBodySerializer getSerializer(SerializationStage stage, SerializationType type, String contentType, IFunction1NoR<ExtendedMap<String, Object>> propertiesConfigurer)
    {
        var event = FindBodySerializerEvent.builder()
                .type(type)
                .stage(stage)
                .httpContentType(contentType)
                .build()
        ;

        if (propertiesConfigurer != null)
        {
            propertiesConfigurer.apply(event.getCustomAttributes());
        }

        eventFindSerializer.next(event);

        ExceptionsUtils.IfNull(
                event.getResult(),
                RuntimeException.class,
                "Can't find body serializer for content-type", event.getHttpContentType(), "java type", event.getType(), "and properties", event.getCustomAttributes()
        );

        return event.getResult();
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class FindSerializerEventFilter implements IFunction1<FindBodySerializerEvent, Boolean> {

        @Internal
        public IFunction1<FindBodySerializerEvent, Boolean> customFilter;

        @Internal
        public Set<Class<?>> availableTypes = new HashSet<>();

        @Internal
        public Set<SerializationStage> serializationStages = new HashSet<>(Arrays.asList(SerializationStage.values()));

        @Internal
        public FilteringList contentTypeFiltering = new FilteringList();

        @Override
        public Boolean applyUnsafe(FindBodySerializerEvent event) throws Throwable
        {
            if (!availableTypes.isEmpty() && !availableTypes.contains(event.getType().getJavaType()))
                return false;

            if (customFilter != null && !Boolean.TRUE.equals(customFilter.apply(event)))
                return false;

            if (!contentTypeFiltering.isEmpty() && !contentTypeFiltering.isMatches(event.getHttpContentType()))
                return false;

            if (event.getStage() != null && !CollectionsSWL.isNullOrEmpty(serializationStages) && !serializationStages.contains(event.getStage()))
                return false;

            return true;
        }

        @Tolerate
        public FindSerializerEventFilter setStages(SerializationStage... types)
        {
            this.serializationStages = new HashSet<>(Arrays.asList(types));
            return this;
        }

        public FindSerializerEventFilter addContentTypeExclusion(String expression)
        {
            this.contentTypeFiltering.exclude(expression);
            return this;
        }

        public FindSerializerEventFilter addContentTypeInclusion(String expression)
        {
            this.contentTypeFiltering.include(expression);
            return this;
        }

        public FindSerializerEventFilter addType(Class<?> classType)
        {
            if (classType != null)
                this.availableTypes.add(classType);

            return this;
        }

        public FindSerializerEventFilter addCustomFilter(IFunction1<FindBodySerializerEvent, Boolean> filterFun)
        {
            if (filterFun != null)
            {
                if (this.customFilter == null)
                    this.customFilter = filterFun;
                else
                    this.customFilter = this.customFilter.and(filterFun);
            }

            return this;
        }
    }
}
