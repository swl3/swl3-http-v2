package ru.swayfarer.swl3.httpv2.serialization;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class AbstractBodySerializer implements IBodySerializerProvider
{
    @Internal
    public IFunction1<FindBodySerializerEvent, Boolean> filterFun;

    @Internal
    public IFunction1<FindBodySerializerEvent, IBodySerializer> bodySerializerProvideFun;

    @Override
    public void provide(FindBodySerializerEvent event) throws Throwable
    {
        if (filterFun != null && !Boolean.TRUE.equals(filterFun.apply(event)))
            return;

        event.setResult(bodySerializerProvideFun.apply(event));
    }
}
