package ru.swayfarer.swl3.httpv2.serialization;

import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;

public class PlainTextSerializer implements IBodySerializer {

    @Override
    public Object deserialize(DataInStream dis, SerializationType type, IHttpResponse response)
    {
        return dis.readAllAsString();
    }

    @Override
    public void serialize(Object instance, DataOutStream dos, SerializationType type, IWritableHttpRequest request)
    {
        dos.writeString(String.valueOf(instance));
    }
}
