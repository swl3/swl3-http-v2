package ru.swayfarer.swl3.httpv2.serialization;

import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;

import java.io.InputStream;

public class RawStreamSerializer implements IBodySerializer
{
    @Override
    public Object deserialize(DataInStream dis, SerializationType type, IHttpResponse response)
    {
        return dis;
    }

    @Override
    public void serialize(Object instance, DataOutStream dos, SerializationType type, IWritableHttpRequest request)
    {
        if (instance instanceof InputStream)
        {
            StreamsUtils.copy().start((InputStream) instance, dos);
            dos.flush();
        }
    }
}
