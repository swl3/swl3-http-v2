package ru.swayfarer.swl3.httpv2;

import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.streams.DataInStream;

public interface IHttpResponse
{
    public HttpHeadersContainer headers();
    public ExtendedOptional<DataInStream> body();
    public HttpCode code();
    public String message();

    public default void ifCode(IFunction1<HttpCode, Boolean> filter, IFunction1NoR<IHttpResponse> action)
    {
        if (filter.apply(code()))
        {
            action.apply(this);
        }
    }

    public default void ifCodeOrElse(
            IFunction1<HttpCode, Boolean> filter,
            IFunction1NoR<IHttpResponse> action,
            IFunction1NoR<IHttpResponse> elseAction
    )
    {
        if (filter.apply(code()))
        {
            if (action != null)
                action.apply(this);
        }
        else
        {
            if (elseAction != null)
                elseAction.apply(this);
        }
    }

    public static interface IWritableHttpResponse extends IHttpResponse{
        public IWritableHttpResponse setHeaders(HttpHeadersContainer headers);
        public IWritableHttpResponse setBody(DataInStream stream);
        public IWritableHttpResponse setBody(byte[] bodyBytes);
        public IWritableHttpResponse setCode(HttpCode code);
        public IWritableHttpResponse setMessage(String message);
    }
}
