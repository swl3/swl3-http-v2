package ru.swayfarer.swl3.httpv2;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class HttpHeadersContainer
{
    @Internal
    public ExtendedMap<String, ExtendedList<String>> rawHeaders = new ExtendedMap<>();

    public HttpHeadersContainer addHeader(String key, String value)
    {
        rawHeaders.getOrCreate(key.toLowerCase(), ExtendedList::new).add(value);
        return this;
    }

    public ExtendedOptional<String> getFirstHeader(String key)
    {
        return ExtendedOptional.of(getHeaders(key.toLowerCase()).map(ExtendedList::first).orNull());
    }

    public ExtendedOptional<ExtendedList<String>> getHeaders(String key)
    {
        return ExtendedOptional.of(rawHeaders.get(key.toLowerCase()));
    }

    public HttpHeadersContainer setHeaders(String key, String... values)
    {
        return setHeaders(key, CollectionsSWL.iterable(values));
    }

    public HttpHeadersContainer setHeaders(String key, Iterable<String> values)
    {
        if (key != null && values != null)
        {
            rawHeaders.put(key.toLowerCase(), CollectionsSWL.list(values));
        }

        return this;
    }
}
