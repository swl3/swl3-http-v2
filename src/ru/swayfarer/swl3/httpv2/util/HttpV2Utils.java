package ru.swayfarer.swl3.httpv2.util;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.httpv2.client.HttpClient;
import ru.swayfarer.swl3.httpv2.client.HttpClientSettings;
import ru.swayfarer.swl3.httpv2.client.factory.HttpClientCreationException;
import ru.swayfarer.swl3.httpv2.client.factory.HttpClientFactory;
import ru.swayfarer.swl3.httpv2.client.factory.HttpClientFactory.ClientBuilder;
import ru.swayfarer.swl3.httpv2.module.DefaultHttpModule;
import ru.swayfarer.swl3.httpv2.module.HttpV2Module;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

public class HttpV2Utils
{
    public static HttpV2Utils INSTANCE = new HttpV2Utils();

    @Internal
    @NonNull
    public HttpClientFactory clientFactory = new HttpClientFactory(this);

    @Internal
    @NonNull
    public UrlMetaInfoParser urlParser = new UrlMetaInfoParser();

    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public HttpV2Utils()
    {
        this(ReflectionsUtils.getInstance());
    }

    public HttpV2Utils(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
        registerModule(new DefaultHttpModule());
    }

    public <Client_Type> ClientBuilder<Client_Type> buildClient(Class<Client_Type> clientClass)
    {
        var builder = new ClientBuilder<Client_Type>();
        builder.clientClass = clientClass;
        builder.buildFun = clientFactory::build;

        return builder;
    }

    public <Client_Type> Client_Type parseHttpClient(Class<Client_Type> clientClass)
    {
        return parseHttpClient(clientClass, null);
    }

    public <Client_Type> Client_Type parseHttpClient(
            Class<Client_Type> clientClass,
            IFunction1NoR<HttpClientSettings<Client_Type>> customConfigurer
    )
    {
        var urlString = reflectionsUtils.annotations().findProperty()
                .name("host")
                .marker(HttpClient.class)
                .type(String.class)
                .in(clientClass)
                .optional().orElseThrow(() -> new HttpClientCreationException("Missing 'host' property at class"))
        ;

        var uriString = reflectionsUtils.annotations().findProperty()
                .name("uri")
                .marker(HttpClient.class)
                .type(String.class)
                .in(clientClass)
                .orNull()
        ;

        if (!StringUtils.isBlank(uriString))
        {
            urlString += uriString;
        }

        var urlMeta = parseUrlMeta(urlString);

        return buildClient(clientClass)
                .host(urlMeta.getProtocol() + "://" + urlMeta.getHost())
                .uri(urlMeta.getUriMapping())
                .postProcess(customConfigurer)
                .port(urlMeta.getPort())
                .build()
        ;
    }

    @SneakyThrows
    public void disableHttpsVerification()
    {
        var trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {  }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {  }

                }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        var allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    public void registerModule(HttpV2Module module)
    {
        clientFactory.eventSettingsCreation.subscribe().by(module::startEnrich);
    }

    public static HttpV2Utils getInstance()
    {
        return INSTANCE;
    }

    public UrlMetaInfo parseUrlMeta(String urlString)
    {
        return urlParser.parse(urlString);
    }
}
