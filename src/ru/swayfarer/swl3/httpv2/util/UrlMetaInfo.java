package ru.swayfarer.swl3.httpv2.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class UrlMetaInfo
{
    public String uriMapping;
    public int port;
    public String host;
    public String protocol;
    public String login;
    public String password;
}
