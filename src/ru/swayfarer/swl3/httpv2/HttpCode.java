package ru.swayfarer.swl3.httpv2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class HttpCode
{
    @Internal
    public int rawStatus;

    public boolean isSuccess()
    {
        return inRange(200);
    }

    public boolean isRedirect()
    {
        return inRange(300);
    }

    public boolean isClientError()
    {
        return inRange(400);
    }

    public boolean isServerError()
    {
        return inRange(500);
    }

    public boolean isError()
    {
        return isClientError() || isServerError();
    }

    public boolean inRange(int bound)
    {
        return rawStatus >= bound && rawStatus <= bound + 99;
    }

    public boolean isBetween(int min, int max)
    {
        return rawStatus >= min && rawStatus <= max;
    }
}
