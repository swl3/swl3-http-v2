package ru.swayfarer.swl3.httpv2;

import ru.swayfarer.swl3.httpv2.IHttpResponse.IWritableHttpResponse;

public interface IHttpRequestExecutor
{
    public IWritableHttpResponse execute(IHttpRequest request);
}
