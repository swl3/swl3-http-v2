package ru.swayfarer.swl3.httpv2.client;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.proxy.ProxyInvocationEvent;
import ru.swayfarer.swl3.asmv2.proxy.ReflectionProxyHelper;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.httpv2.HttpAddress;
import ru.swayfarer.swl3.httpv2.HttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent.IRequestEnricher;
import ru.swayfarer.swl3.httpv2.client.factory.HttpClientCreationException;
import ru.swayfarer.swl3.httpv2.client.response.enrich.HttpResponseEnrichEvent;
import ru.swayfarer.swl3.httpv2.client.response.enrich.HttpResponseEnrichEvent.IResponseEnricher;
import ru.swayfarer.swl3.httpv2.util.HttpV2Utils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class ClientProxyGenerator
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public HttpV2Utils httpV2Utils;

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    @Internal
    @NonNull
    public IObservable<HttpRequestEnrichEvent<?>> eventRequestEnrich = Observables.createObservable()
            .exceptions().throwOnFail()
    ;

    @Internal
    @NonNull
    public IObservable<HttpResponseEnrichEvent<?>> eventResponseEnrich = Observables.createObservable()
            .exceptions().throwOnFail()
    ;

    public ClientProxyGenerator(
            @NonNull AsmUtilsV2 asmUtils,
            @NonNull ReflectionsUtils reflectionsUtils,
            @NonNull HttpV2Utils httpV2Utils
    )
    {
        this.asmUtils = asmUtils;
        this.reflectionsUtils = reflectionsUtils;
        this.httpV2Utils = httpV2Utils;
    }

    public <ClientInterface> ClientInterface createClient(HttpClientSettings<ClientInterface> settings)
    {
        var clientClass = settings.getClientClass();
        var proxyHelper = new ReflectionProxyHelper(asmUtils, reflectionsUtils);
        var methodToRequestFactoryMap = new ExtendedMap<Method, RequestMethodInfo>().concurrent();

        return proxyHelper.newProxyInstance(
                (invocationEvent) -> {
                    var overrideMethod = invocationEvent.getOverrideMethod();
                    var requestMethodInfo = methodToRequestFactoryMap.getOrCreate(overrideMethod, () -> {
                        var requestEnrichEvent = new HttpRequestEnrichEvent<ClientInterface>();
                        requestEnrichEvent.setOverrideMethod(overrideMethod);
                        requestEnrichEvent.setSettings(settings);

                        eventRequestEnrich.next(requestEnrichEvent);

                        var newRequestMethodInfo = new RequestMethodInfo((event) -> {
                            var result = new HttpRequest();
                            var target = new HttpAddress();
                            target.setHost(settings.getTargetHost());
                            result.setTarget(target);

                            for (var requestEnricher : requestEnrichEvent.getRequestEnrichers())
                            {
                                requestEnricher.enrich(result, event);
                            }

                            return result;
                        });

                        newRequestMethodInfo.setRequestEnrichers(requestEnrichEvent.getRequestEnrichers());

                        var responseEnrichEvent = new HttpResponseEnrichEvent<ClientInterface>();
                        responseEnrichEvent.setClientSettings(settings);
                        responseEnrichEvent.setOverrideMethod(overrideMethod);
                        eventResponseEnrich.next(responseEnrichEvent);

                        newRequestMethodInfo.setResponseEnrichers(responseEnrichEvent.getResponseEnrichers());

                        return newRequestMethodInfo;
                    });

                    try
                    {
                        var requestFactory = requestMethodInfo.getRequestFactory();
                        var request = requestFactory.apply(invocationEvent);
                        settings.getEventRequestEnrich().next(request.asWritable().get());

                        var response = settings.getRequestExecutor().execute(request);
                        while (response.code().isRedirect())
                        {
                            var newHost = response.headers().getFirstHeader("Location").orNull();
                            if (StringUtils.isBlank(newHost))
                            {
                                break;
                            }

                            var urlMetaInfo = httpV2Utils.parseUrlMeta(newHost);
                            request.target().setHost(urlMetaInfo.getProtocol() + "://" + urlMetaInfo.getHost());
                            request.target().setUri(urlMetaInfo.getUriMapping());
                            request.target().setPort(urlMetaInfo.getPort());

                            response = settings.getRequestExecutor().execute(request);
                        }

                        for (var responseEnricher : requestMethodInfo.getResponseEnrichers())
                        {
                            responseEnricher.enrichResponse(request, response, invocationEvent);
                        }
                    }
                    catch (Throwable e)
                    {
                        throw new HttpClientException("Error while executing request from method " + overrideMethod, e);
                    }
                },
                (cfg) -> cfg.addInterface(clientClass)
        );
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @Internal
    public static class QueryParam {
        public int paramIndex;
        public String name;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @Internal
    public static class RequestMethodInfo {
        @NonNull
        public IFunction1<ProxyInvocationEvent, IHttpRequest> requestFactory;

        public ExtendedList<IRequestEnricher> requestEnrichers = new ExtendedList<>();
        public ExtendedList<IResponseEnricher> responseEnrichers = new ExtendedList<>();

        public RequestMethodInfo(@NonNull IFunction1<ProxyInvocationEvent, IHttpRequest> requestFactory)
        {
            this.requestFactory = requestFactory;
        }
    }
}
