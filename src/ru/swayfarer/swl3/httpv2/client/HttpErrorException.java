package ru.swayfarer.swl3.httpv2.client;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.httpv2.HttpCode;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Getter
@Setter
@Accessors(chain = true)
public class HttpErrorException extends RuntimeException
{
    @Internal
    @NonNull
    public String httpMessage;

    @Internal
    @NonNull
    public String bodyString;

    @Internal
    @NonNull
    public HttpCode code;

    public HttpErrorException(@NonNull IHttpResponse response)
    {
        this(response.message(), response.code());
        bodyString = response.body().map(DataInStream::readAllAsString).orNull();
    }

    public HttpErrorException(String message, HttpCode code)
    {
        this.httpMessage = message;
        this.code = code;
    }

    @Override
    public String getMessage()
    {
        var buffer = new DynamicString();
        buffer.append("code: ");
        buffer.append(code.getRawStatus());
        buffer.append(".");

        if (!StringUtils.isBlank(httpMessage))
        {
            buffer.append(" message: ");
            buffer.append(httpMessage);
            buffer.append(".");
        }

        if (!StringUtils.isBlank(bodyString))
        {
            buffer.append(" body: ");
            if (bodyString.length() > 50)
                buffer.newLine();

            buffer.append(bodyString);
        }

        return buffer.toString();
    }
}
