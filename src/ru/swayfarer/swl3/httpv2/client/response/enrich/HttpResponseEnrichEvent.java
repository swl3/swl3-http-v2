package ru.swayfarer.swl3.httpv2.client.response.enrich;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.proxy.ProxyInvocationEvent;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.httpv2.IHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse.IWritableHttpResponse;
import ru.swayfarer.swl3.httpv2.client.HttpClientSettings;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class HttpResponseEnrichEvent<Client_Interface>
{
    @Internal
    @NonNull
    public HttpClientSettings<Client_Interface> clientSettings;

    @Internal
    @NonNull
    public ExtendedMap<String, Object> customProperties = new ExtendedMap<>().synchronize();

    @Internal
    @NonNull
    public Method overrideMethod;

    @Internal
    @NonNull
    public ExtendedList<IResponseEnricher> responseEnrichers = new ExtendedList<>();

    public static interface IResponseEnricher {
        public void enrichResponse(IHttpRequest request, IWritableHttpResponse response, ProxyInvocationEvent invocationEvent);
    }
}
