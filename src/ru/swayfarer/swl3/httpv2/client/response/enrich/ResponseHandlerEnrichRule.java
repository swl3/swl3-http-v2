package ru.swayfarer.swl3.httpv2.client.response.enrich;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.client.IResponseHandler;
import ru.swayfarer.swl3.httpv2.client.ResponseHandler;
import ru.swayfarer.swl3.httpv2.client.request.enrich.IncorrectHttpClientException;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
@Accessors(chain = true)
public class ResponseHandlerEnrichRule implements IResponseEnrichRule, ResponseEnrichEventProperties
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public static String responseHandleType = "Response Handler";

    public ResponseHandlerEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpResponseEnrichEvent<?> event)
    {
        var overrideMethod = event.getOverrideMethod();
        var params = overrideMethod.getParameters();
        var responseHandlerParamIndex = new AtomicInteger(-1);

        for (var paramIndex = 0; paramIndex < params.length; paramIndex ++)
        {
            var param = params[paramIndex];

            var responseHandlerAnnotation = reflectionsUtils.annotations().findAnnotation()
                    .ofType(ResponseHandler.class)
                    .in(param)
                    .get()
            ;


            if (responseHandlerAnnotation != null)
            {
                ExceptionsUtils.If(
                        responseHandlerParamIndex.get() != -1,
                        IncorrectHttpClientException.class,
                        "Duplicate annotation", ResponseHandler.class, "at method", overrideMethod, ". The request can has only one storer!"
                );

                responseHandlerParamIndex.set(paramIndex);
            }
        }

        if (responseHandlerParamIndex.get() != -1)
        {
            var properties = event.getCustomProperties();
            var responseHandleType = properties.get(propertyResponseHandleType);

            ExceptionsUtils.IfNot(
                    StringUtils.isBlank(responseHandleType),
                    IncorrectHttpClientException.class,
                    "Duplicate response handle type at method", overrideMethod, ": '", responseHandleType, "' and '", responseHandleType, "'"
            );

            properties.put(propertyResponseHandleType, ResponseHandlerEnrichRule.responseHandleType);

            event.getResponseEnrichers().add((request, response, invocationEvent) -> {
                var responseHandler = (IResponseHandler) invocationEvent.getArg(responseHandlerParamIndex.get());
                ExceptionsUtils.IfNull(responseHandler, IllegalArgumentException.class, "Http request got a null response handler! Its not allowed!");

                responseHandler.handle(request, response);
            });
        }
    }
}
