package ru.swayfarer.swl3.httpv2.client.response.enrich;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.httpv2.client.HttpErrorException;
import ru.swayfarer.swl3.httpv2.client.request.enrich.IncorrectHttpClientException;
import ru.swayfarer.swl3.httpv2.serialization.FindBodySerializerEvent.SerializationStage;
import ru.swayfarer.swl3.httpv2.serialization.SerializationType;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

public class ResponseReturnEnrichRule implements IResponseEnrichRule, ResponseEnrichEventProperties
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public static String responseHandleType = "As method result";

    public ResponseReturnEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpResponseEnrichEvent<?> event)
    {
        var overrideMethod = event.getOverrideMethod();

        var returnType = overrideMethod.getReturnType();
        var enrichers = event.getResponseEnrichers();

        var properties = event.getCustomProperties();
        var handleType = properties.get(propertyResponseHandleType);

        ExceptionsUtils.IfNot(
                StringUtils.isBlank(handleType),
                IncorrectHttpClientException.class,
                "Duplicate response handle type at method", overrideMethod, ": '", handleType, "' and '", responseHandleType, "'"
        );

        properties.put(propertyResponseHandleType, responseHandleType);

        if (returnType.equals(IHttpResponse.class))
        {
            enrichers.add((request, response, invocationHandler) -> {
                invocationHandler.setReturnObj(response);
            });
        }
        else
        {
            var serializationType = SerializationType.builder()
                    .genericType(overrideMethod.getGenericReturnType())
                    .javaType(overrideMethod.getReturnType())
                    .build()
            ;

            enrichers.add((request, response, invocationHandler) -> {
                if (response.code().isError())
                {
                    ExceptionsUtils.throwException(new HttpErrorException(response));
                }

                if (returnType.equals(void.class))
                    return;

                var headers = response.headers();
                event.getClientSettings().getBodySerializerForHeaders(SerializationStage.Deserialize,serializationType, headers)
                        .ifPresentOrElseThrow(
                                (serializer) -> {
                                    var readedValue = serializer.deserialize(response.body().get(), serializationType, response);
                                    invocationHandler.setReturnObj(readedValue);
                                }
                        );
            });
        }
    }
}
