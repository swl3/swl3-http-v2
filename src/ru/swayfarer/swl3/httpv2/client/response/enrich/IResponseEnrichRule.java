package ru.swayfarer.swl3.httpv2.client.response.enrich;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

public interface IResponseEnrichRule extends IFunction1NoR<HttpResponseEnrichEvent<?>>
{
    public void enrich(HttpResponseEnrichEvent<?> event);

    @Override
    default void applyNoRUnsafe(HttpResponseEnrichEvent<?> httpResponseEnrichEvent) throws Throwable
    {
        enrich(httpResponseEnrichEvent);
    }
}
