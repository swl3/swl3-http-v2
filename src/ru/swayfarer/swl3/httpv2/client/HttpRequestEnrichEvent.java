package ru.swayfarer.swl3.httpv2.client;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.proxy.ProxyInvocationEvent;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class HttpRequestEnrichEvent<Client_Interface> extends AbstractEvent
{
    @Internal
    @NonNull
    public HttpClientSettings<Client_Interface> settings;

    @Internal
    @NonNull
    public Method overrideMethod;

    @Internal
    @NonNull
    public ExtendedList<IRequestEnricher> requestEnrichers = new ExtendedList<>();

    public static interface IRequestEnricher
    {
        public void enrich(IWritableHttpRequest request, ProxyInvocationEvent event);
    }
}
