package ru.swayfarer.swl3.httpv2.client.factory;

public class HttpClientCreationException extends RuntimeException
{
    public HttpClientCreationException()
    {
    }

    public HttpClientCreationException(String message)
    {
        super(message);
    }

    public HttpClientCreationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public HttpClientCreationException(Throwable cause)
    {
        super(cause);
    }
}
