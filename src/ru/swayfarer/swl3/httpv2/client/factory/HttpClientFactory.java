package ru.swayfarer.swl3.httpv2.client.factory;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.httpv2.URLHttpRequestExecutor;
import ru.swayfarer.swl3.httpv2.client.ClientProxyGenerator;
import ru.swayfarer.swl3.httpv2.client.HttpClientSettings;
import ru.swayfarer.swl3.httpv2.client.request.enrich.DefaultEnrichRules;
import ru.swayfarer.swl3.httpv2.util.HttpV2Utils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class HttpClientFactory
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public ClientProxyGenerator clientProxyGenerator;

    @Internal
    @NonNull
    public HttpV2Utils httpV2Utils;

    @Internal
    @NonNull
    public IObservable<ClientSettingsCreationEvent<?>> eventSettingsCreation = Observables.createObservable()
            .exceptions().configure((eh) -> {
                eh.configure()
                        .rule()
                        .wrap(HttpClientCreationException.class)
                        .thanSkip()
                        .throwEx()
                ;
            })
    ;

    public HttpClientFactory()
    {
        this(HttpV2Utils.getInstance());
    }

    public HttpClientFactory(@NonNull HttpV2Utils httpV2Utils)
    {
        this(httpV2Utils, AsmUtilsV2.getInstance(), ReflectionsUtils.getInstance());
    }

    public HttpClientFactory(@NonNull HttpV2Utils httpV2Utils, @NonNull AsmUtilsV2 asmUtils, @NonNull ReflectionsUtils reflectionsUtils)
    {
        this.asmUtils = asmUtils;
        this.reflectionsUtils = reflectionsUtils;
        this.httpV2Utils = httpV2Utils;

        clientProxyGenerator = new ClientProxyGenerator(asmUtils, reflectionsUtils, httpV2Utils);
        DefaultEnrichRules.registerDefaultRules(clientProxyGenerator);
    }

    @Internal
    public <Client_Type> Client_Type build(ClientBuilder<Client_Type> builder)
    {
        var settings = new HttpClientSettings<Client_Type>();

        if (builder.uri != null)
            settings.targetPath = builder.uri;

        if (builder.target != null)
            settings.targetHost = builder.target;

        if (builder.port != -1)
            settings.port = builder.port;

        var creationEvent = new ClientSettingsCreationEvent<Client_Type>();
        creationEvent.setResult(settings);
        eventSettingsCreation.next(creationEvent);

        if (StringUtils.isBlank(settings.defaultContentType))
            settings.setDefaultContentType("text/plain");

        if (settings.getRequestExecutor() == null)
            settings.setRequestExecutor(new URLHttpRequestExecutor());

        settings.setClientClass(builder.clientClass);

        if (builder.postProcessFun != null)
        {
            builder.postProcessFun.apply(settings);
        }

        return clientProxyGenerator.createClient(settings);
    }

    public static class ClientBuilder<Client_Type> {
        public Class<Client_Type> clientClass;
        public String target;
        public String uri;
        public int port = -1;
        public IFunction1NoR<HttpClientSettings<Client_Type>> postProcessFun;

        public IFunction1<ClientBuilder<Client_Type>, Client_Type> buildFun;

        public ClientBuilder<Client_Type> host(String target)
        {
            this.target = target;
            return this;
        }

        public ClientBuilder<Client_Type> uri(String uri)
        {
            this.uri = uri;
            return this;
        }

        public ClientBuilder<Client_Type> postProcess(IFunction1NoR<HttpClientSettings<Client_Type>> postProcessFun)
        {
            this.postProcessFun = postProcessFun;
            return this;
        }

        public ClientBuilder<Client_Type> port(int port)
        {
            this.port = port;
            return this;
        }

        public Client_Type build()
        {
            return buildFun.apply(this);
        }
    }
}
