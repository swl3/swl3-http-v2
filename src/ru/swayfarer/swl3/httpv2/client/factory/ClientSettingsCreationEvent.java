package ru.swayfarer.swl3.httpv2.client.factory;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.httpv2.client.HttpClientSettings;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

@Getter
@Setter
@Accessors
public class ClientSettingsCreationEvent<Client_Interface> extends AbstractEvent
{
    @Internal
    @NonNull
    public HttpClientSettings<Client_Interface> result;
}
