package ru.swayfarer.swl3.httpv2.client;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface HttpHeaders
{
    public String[] value() default {};
    public HttpHeader[] headers() default {};
}
