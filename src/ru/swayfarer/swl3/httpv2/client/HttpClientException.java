package ru.swayfarer.swl3.httpv2.client;

public class HttpClientException extends RuntimeException
{
    public HttpClientException()
    {
    }

    public HttpClientException(String message)
    {
        super(message);
    }

    public HttpClientException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public HttpClientException(Throwable cause)
    {
        super(cause);
    }
}
