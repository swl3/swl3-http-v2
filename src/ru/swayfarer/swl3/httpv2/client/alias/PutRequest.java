package ru.swayfarer.swl3.httpv2.client.alias;

import ru.swayfarer.swl3.httpv2.client.HttpRequest;
import ru.swayfarer.swl3.reflection.annotations.AnnotationAlias;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@HttpRequest(path = "none", method = "PUT")
public @interface PutRequest
{
    @AnnotationAlias(value = HttpRequest.class, name = "path")
    public String value() default "/";
}
