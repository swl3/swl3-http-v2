package ru.swayfarer.swl3.httpv2.client;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface HttpRequest
{
    public String method();
    public String path() default "/";
}
