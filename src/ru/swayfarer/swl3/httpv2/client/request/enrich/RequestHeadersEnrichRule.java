package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.proxy.ProxyInvocationEvent;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.client.HttpHeader;
import ru.swayfarer.swl3.httpv2.client.HttpHeaderParam;
import ru.swayfarer.swl3.httpv2.client.HttpHeaders;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

public class RequestHeadersEnrichRule implements IRequestEnrichRule
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public RequestHeadersEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var overrideMethod = event.getOverrideMethod();
        var allHeaders = new ExtendedList<Header>();

        readStringHeadersParam(overrideMethod, allHeaders);
        readHeaderAnnotationsParam(overrideMethod, allHeaders);

        var variableNameToIndex = new ExtendedMap<String, Integer>();
        var params = overrideMethod.getParameters();

        for (var paramIndex = 0; paramIndex < params.length; paramIndex ++)
        {
            var param = params[paramIndex];

            var headerParamName = reflectionsUtils.annotations().findProperty()
                    .name("name")
                    .type(String.class)
                    .marker(HttpHeaderParam.class)
                    .in(param)
                    .get()
            ;

            variableNameToIndex.put(headerParamName, paramIndex);
        }

        event.getRequestEnrichers().add((request, invocationHandler) -> {
            var headers = request.headers();

            for (var header : allHeaders)
            {
                headers.addHeader(
                        replaceAllVars(header.getName(), variableNameToIndex, invocationHandler),
                        replaceAllVars(header.getValue(), variableNameToIndex, invocationHandler)
                );
            }
        });
    }

    private void readStringHeadersParam(java.lang.reflect.Method overrideMethod, ExtendedList<Header> allHeaders)
    {
        reflectionsUtils.annotations().findProperty()
                .name("value")
                .type(String[].class)
                .marker(HttpHeaders.class)
                .in(overrideMethod)
                .optional().ifPresent((headersStrs) -> {
                    for (var headerStr : headersStrs)
                    {
                        var parts = headerStr.split(":");
                        ExceptionsUtils.If(
                                parts.length < 2,
                                IncorrectHttpClientException.class,
                                "Incorrect header str '", headerStr, "' in method", overrideMethod, "! There is must be Key: Value construction split by ':' symbol!"
                        );

                        var key = StringUtils.strip(parts[0]);
                        var value = StringUtils.strip(
                                StringUtils.concatWith(":", 1, -1, (Object[]) parts)
                        );

                        var header = new Header();
                        header.setName(key);
                        header.setValue(value);

                        allHeaders.add(header);
                    }
                 }
         );
    }

    private void readHeaderAnnotationsParam(java.lang.reflect.Method overrideMethod, ExtendedList<Header> allHeaders)
    {
        reflectionsUtils.annotations()
                .findProperty()
                    .type(HttpHeader[].class)
                    .name("headers")
                    .marker(HttpHeaders.class)
                    .in(overrideMethod)
                    .optional().ifPresent((headers) -> {
                        for (var headerAnnotation : headers)
                        {
                            var header = new Header();
                            header.setName(headerAnnotation.name());
                            header.setValue(headerAnnotation.value());
                            allHeaders.add(header);
                        }
                    })
        ;
    }

    public String replaceAllVars(String str, ExtendedMap<String, Integer> nameToIndex, ProxyInvocationEvent invocationEvent)
    {
        for (var variable : nameToIndex)
        {
            var name = variable.getKey();
            var index = variable.getValue();
            str = str.replace("{" + name + "}", String.valueOf(invocationEvent.getArg(index)));
        }

        return str;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class Header {

        @NonNull
        @Internal
        public String name;

        @NonNull
        @Internal
        public String value;
    }
}
