package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.proxy.ProxyInvocationEvent;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.client.HttpUrlPart;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

public class RequestPathPartsEnrichRule implements IRequestEnrichRule
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    public RequestPathPartsEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var overrideMethod = event.getOverrideMethod();
        var params = overrideMethod.getParameters();
        var urlParts = new ExtendedList<UrlPart>();

        for (var paramIndex = 0; paramIndex < params.length; paramIndex ++)
        {
            var param = params[paramIndex];
            var urlPartName = reflectionsUtils.annotations()
                    .findProperty()
                    .type(String.class)
                    .name("name")
                    .marker(HttpUrlPart.class)
                    .in(param)
                    .get()
            ;

            if (urlPartName != null)
            {
                if (StringUtils.isBlank(urlPartName))
                {
                    ExceptionsUtils.If(
                            param.isNamePresent(),
                            IncorrectHttpClientException.class,
                            "Can't resolve parameter name of http client method", overrideMethod
                    );

                    urlPartName = param.getName();
                }

                ExceptionsUtils.If(
                        StringUtils.isBlank(urlPartName),
                        IncorrectHttpClientException.class,
                        "Method parameter at index", paramIndex, "annotated by", HttpUrlPart.class, "but param name is blank! Its not allowed", overrideMethod
                );

                var urlPart = new UrlPart();
                urlPart.setPartName(urlPartName);
                urlPart.setParamIndex(paramIndex);
                urlParts.add(urlPart);
            }
        }

        event.getRequestEnrichers().add((request, invocationEvent) -> {
            var target = request.target();
            var newHost = replaceQueryParams(target.getHost(), urlParts, invocationEvent);
            var newUri = replaceQueryParams(target.getUri(), urlParts, invocationEvent);

            target.setHost(newHost);
            target.setUri(newUri);
        });
    }

    private String replaceQueryParams(String original, ExtendedList<UrlPart> urlParts, ProxyInvocationEvent invocationEvent)
    {
        for (var urlPart : urlParts)
        {
            original = original.replace(
                    "{" + urlPart.getPartName() + "}",
                    String.valueOf(
                            invocationEvent.getArg(urlPart.getParamIndex())
                    )
            );
        }

        return original;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class UrlPart {
        public int paramIndex;
        public String partName;
    }
}
