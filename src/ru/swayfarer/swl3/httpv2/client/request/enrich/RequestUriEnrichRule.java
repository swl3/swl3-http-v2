package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

public class RequestUriEnrichRule implements IRequestEnrichRule
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var overrideMethod = event.getOverrideMethod();
        
    }
}
