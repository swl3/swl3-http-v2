package ru.swayfarer.swl3.httpv2.client.request.enrich;

public class IncorrectHttpClientException extends RuntimeException
{
    public IncorrectHttpClientException()
    {
    }

    public IncorrectHttpClientException(String message)
    {
        super(message);
    }

    public IncorrectHttpClientException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IncorrectHttpClientException(Throwable cause)
    {
        super(cause);
    }
}
