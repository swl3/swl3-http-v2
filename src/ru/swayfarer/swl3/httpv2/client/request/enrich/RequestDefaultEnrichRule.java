package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.httpv2.client.HttpRequest;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

public class RequestDefaultEnrichRule implements IRequestEnrichRule
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public RequestDefaultEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var overrideMethod = event.getOverrideMethod();

        var requestMethod = reflectionsUtils.annotations().findProperty()
                .type(String.class)
                .name("method")
                .marker(HttpRequest.class)
                .in(overrideMethod)
                .get()
        ;

        var requestUri = reflectionsUtils.annotations().findProperty()
                .type(String.class)
                .name("path")
                .marker(HttpRequest.class)
                .in(overrideMethod)
                .get()
        ;

        var clientSettings = event.getSettings();

        event.getRequestEnrichers().add((request, invocationEvent) -> {
            request.target()
                    .setMethod(requestMethod)
                    .setUri(requestUri)
            ;

            if (clientSettings.port != -1)
                request.target().setPort(clientSettings.port);
        });
    }
}
