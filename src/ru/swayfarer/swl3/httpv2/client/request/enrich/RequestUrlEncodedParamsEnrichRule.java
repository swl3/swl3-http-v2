package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;
import ru.swayfarer.swl3.httpv2.UrlEncodedParams;
import ru.swayfarer.swl3.httpv2.client.HttpClientSettings;
import ru.swayfarer.swl3.httpv2.client.HttpFormParam;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.httpv2.serialization.SerializationType;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import java.lang.reflect.Type;

public class RequestUrlEncodedParamsEnrichRule implements IRequestEnrichRule, RequestEnrichProperties
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    public static String bodyLocation = "Url-encoded params";

    public RequestUrlEncodedParamsEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var overrideMethod = event.getOverrideMethod();
        var enrichers = event.getRequestEnrichers();
        var encodedParams = new ExtendedList<EncodedParam>();
        var parameters = overrideMethod.getParameters();

        for (var paramIndex = 0; paramIndex < parameters.length; paramIndex ++)
        {
            var param = parameters[paramIndex];

            var formParamName = reflectionsUtils.annotations().findProperty()
                    .name("name")
                    .type(String.class)
                    .marker(HttpFormParam.class)
                    .in(param)
                    .get()
            ;

            if (formParamName != null)
            {
                encodedParams.add(
                        EncodedParam.builder()
                            .name(formParamName)
                            .paramIndex(paramIndex)
                            .build()
                );
            }
        }

        if (!CollectionsSWL.isNullOrEmpty(encodedParams))
        {
            var properties = event.getCustomAttributes();
            var handleType = properties.get(propertyBodyLocation);

            ExceptionsUtils.IfNot(
                    StringUtils.isBlank(handleType),
                    IncorrectHttpClientException.class,
                    "Duplicate body source at method", overrideMethod, ": '", handleType, "' and '", bodyLocation, "'"
            );

            properties.put(propertyBodyLocation, bodyLocation);
            var clientSettings = event.getSettings();

            enrichers.add((request, invocationEvent) -> {
                var urlParamsContainer = new UrlEncodedParams();

                for (var urlParam : encodedParams)
                {
                    urlParamsContainer.add(urlParam.getName(), invocationEvent.getArg(urlParam.getParamIndex()));
                }

                request.headers().setHeaders("Content-Type", "application/x-www-form-urlencoded");
                RequestBodyEnrichRule.setObjectBody(clientSettings, request, urlParamsContainer, null);
            });
        }
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @AllArgsConstructor
    @NoArgsConstructor
    @SuperBuilder
    public static class EncodedParam {
        public String name;
        public int paramIndex;
    }
}
