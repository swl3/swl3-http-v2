package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.httpv2.client.ClientProxyGenerator.QueryParam;
import ru.swayfarer.swl3.httpv2.client.HttpQueryParam;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

public class RequestQueryParamsEnrichRule implements IRequestEnrichRule
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    public RequestQueryParamsEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var overrideMethod = event.getOverrideMethod();
        var queryParams = new ExtendedList<QueryParam>();
        var params = overrideMethod.getParameters();

        for (var paramIndex = 0; paramIndex < params.length; paramIndex ++)
        {
            var param = params[paramIndex];
            var paramName = reflectionsUtils.annotations()
                    .findProperty()
                    .marker(HttpQueryParam.class)
                    .name("name")
                    .type(String.class)
                    .in(param)
                    .get()
            ;

            if (paramName != null)
            {
                if (StringUtils.isBlank(paramName))
                {
                    paramName = param.getName();
                }

                var queryParam = new QueryParam();
                queryParam.setParamIndex(paramIndex);
                queryParam.setName(paramName);
                queryParams.add(queryParam);
            }
        }

        event.getRequestEnrichers().add((request, invocationEvent) -> {
            for (var queryParam : queryParams)
            {
                request.target().addQueryParam(
                        queryParam.getName(),
                        String.valueOf(invocationEvent.getArg(queryParam.getParamIndex())))
                ;
            }
        });
    }
}
