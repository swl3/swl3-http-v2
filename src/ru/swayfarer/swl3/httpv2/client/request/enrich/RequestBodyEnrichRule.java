package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.client.HttpBody;
import ru.swayfarer.swl3.httpv2.client.HttpClientSettings;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;
import ru.swayfarer.swl3.httpv2.serialization.FindBodySerializerEvent.SerializationStage;
import ru.swayfarer.swl3.httpv2.serialization.SerializationType;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class RequestBodyEnrichRule implements IRequestEnrichRule, RequestEnrichProperties
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public static String bodyLocation = "HttpBody parameter";

    public RequestBodyEnrichRule(@NonNull ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        var clientSettings = event.getSettings();
        var requestBodyParamIndex = new AtomicInteger(-1);
        var overrideMethod = event.getOverrideMethod();
        var params = overrideMethod.getParameters();
        var requestBodyParam = new AtomicReference<Parameter>();

        for (var paramIndex = 0; paramIndex < params.length; paramIndex ++)
        {
            var param = params[paramIndex];

            var bodyAnnotation = reflectionsUtils.annotations()
                    .findAnnotation()
                    .ofType(HttpBody.class)
                    .in(param)
                    .get()
            ;

            if (bodyAnnotation != null)
            {
                if (requestBodyParamIndex.get() != -1)
                {
                    ExceptionsUtils.throwException(new IllegalStateException("Duplicate " + HttpBody.class + "annotation in method " + overrideMethod));
                }

                requestBodyParam.set(param);
                requestBodyParamIndex.set(paramIndex);
            }
        }

        if (requestBodyParamIndex.get() != -1)
        {
            var properties = event.getCustomAttributes();
            var handleType = properties.get(propertyBodyLocation);

            ExceptionsUtils.IfNot(
                    StringUtils.isBlank(handleType),
                    IncorrectHttpClientException.class,
                    "Duplicate body source at method", overrideMethod, ": '", handleType, "' and '", bodyLocation, "'"
            );

            properties.put(propertyBodyLocation, bodyLocation);

            event.getRequestEnrichers().add((request, invocationEvent) -> {
                var bodyObj = invocationEvent.getArg(requestBodyParamIndex.get());
                if (bodyObj != null)
                {
                    setObjectBody(clientSettings, request, bodyObj, requestBodyParam.get().getParameterizedType());
                }
            });
        }
    }

    public static void setObjectBody(
            HttpClientSettings<?> clientSettings,
            IWritableHttpRequest request,
            Object bodyObject,
            Type bodyGenericType
    )
    {
        var serializationType = SerializationType.builder()
                .javaType(bodyObject.getClass())
                .genericType(bodyGenericType)
                .genericObject(ReflectionsUtils.getInstance().generics().parse(bodyGenericType))
                .build()
        ;

        request.setBodyWriter((dos) -> {
            clientSettings.getBodySerializerForHeaders(SerializationStage.Serialize, serializationType, request.headers())
                    .ifPresentOrElseThrow(
                            (serializer) -> {
                                serializer.serialize(bodyObject, dos, serializationType, request);
                            }
                    );
        });
    }
}
