package ru.swayfarer.swl3.httpv2.client.request.enrich;

import lombok.var;
import ru.swayfarer.swl3.httpv2.client.ClientProxyGenerator;
import ru.swayfarer.swl3.httpv2.client.response.enrich.ResponseHandlerEnrichRule;
import ru.swayfarer.swl3.httpv2.client.response.enrich.ResponseReturnEnrichRule;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

public class DefaultEnrichRules
{
    public static void registerDefaultRules(ClientProxyGenerator generator)
    {
        var reflectionsUtils = ReflectionsUtils.getInstance();
        var eventRequestEnrich = generator.eventRequestEnrich;

        eventRequestEnrich.subscribe().by(new RequestBodyEnrichRule(reflectionsUtils));
        eventRequestEnrich.subscribe().by(new RequestHeadersEnrichRule(reflectionsUtils));
        eventRequestEnrich.subscribe().by(new RequestDefaultEnrichRule(reflectionsUtils), -1);
        eventRequestEnrich.subscribe().by(new RequestPathPartsEnrichRule(reflectionsUtils));
        eventRequestEnrich.subscribe().by(new RequestQueryParamsEnrichRule(reflectionsUtils));
        eventRequestEnrich.subscribe().by(new RequestUrlEncodedParamsEnrichRule(reflectionsUtils));

        var eventResponseEnrich = generator.eventResponseEnrich;

        eventResponseEnrich.subscribe().by(new ResponseHandlerEnrichRule(reflectionsUtils));
        eventResponseEnrich.subscribe().by(new ResponseReturnEnrichRule(reflectionsUtils));
    }
}
