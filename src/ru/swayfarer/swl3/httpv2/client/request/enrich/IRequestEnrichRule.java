package ru.swayfarer.swl3.httpv2.client.request.enrich;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.httpv2.client.HttpRequestEnrichEvent;

public interface IRequestEnrichRule extends IFunction1NoR<HttpRequestEnrichEvent<?>>
{
    @Override
    default void applyNoRUnsafe(HttpRequestEnrichEvent<?> event) throws Throwable
    {
        enrich(event);
    }

    public void enrich(HttpRequestEnrichEvent<?> event) throws Throwable;
}
