package ru.swayfarer.swl3.httpv2.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.ExceptionOptional;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.httpv2.HttpHeadersContainer;
import ru.swayfarer.swl3.httpv2.IHttpRequest.IWritableHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpRequestExecutor;
import ru.swayfarer.swl3.httpv2.serialization.BodySerializerFinder;
import ru.swayfarer.swl3.httpv2.serialization.FindBodySerializerEvent.SerializationStage;
import ru.swayfarer.swl3.httpv2.serialization.IBodySerializer;
import ru.swayfarer.swl3.httpv2.serialization.SerializationType;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class HttpClientSettings<Client_Interface>
{
    @Internal
    @NonNull
    public String targetHost;

    @Internal
    @NonNull
    public String targetPath;

    @Internal
    @NonNull
    public String protocol;

    @Internal
    public int port = -1;

    @Internal
    @NonNull
    public IHttpRequestExecutor requestExecutor;

    @Internal
    @NonNull
    public Class<Client_Interface> clientClass;

    @Internal
    @NonNull
    public String defaultContentType = "text/plain";

    @Internal
    @NonNull
    public BodySerializerFinder bodySerializerFinder = new BodySerializerFinder();

    @Internal
    @NonNull
    public IObservable<IWritableHttpRequest> eventRequestEnrich = Observables.createObservable()
            .exceptions().throwOnFail()
    ;

    public ExceptionOptional<IBodySerializer> getBodySerializer(SerializationStage stage, SerializationType javaType, String contentType)
    {
        return ExceptionOptional.ofAction(() -> bodySerializerFinder.getSerializer(stage, javaType, contentType));
    }

    public ExtendedOptional<IBodySerializer> getBodySerializerForHeaders(SerializationStage stage, SerializationType type, HttpHeadersContainer headers)
    {
        var contentType = getRawContentTypeForRequest(headers);
        return getBodySerializer(stage, type, contentType);
    }

    public String getRawContentTypeForRequest(HttpHeadersContainer headers)
    {
        var contentType = headers.getFirstHeader("Content-Type").orElse(defaultContentType);

        if (StringUtils.isBlank(contentType))
            return "application/json";

        var parts = contentType.split(";");

        if (StringUtils.isBlank(parts[0]))
            return "application/json";

        return parts[0];
    }
}
