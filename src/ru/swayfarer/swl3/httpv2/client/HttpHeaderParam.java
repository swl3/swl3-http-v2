package ru.swayfarer.swl3.httpv2.client;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface HttpHeaderParam
{
    public String name() default "";
}
