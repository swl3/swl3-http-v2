package ru.swayfarer.swl3.httpv2.client;

import ru.swayfarer.swl3.httpv2.IHttpRequest;
import ru.swayfarer.swl3.httpv2.IHttpResponse;

public interface IResponseHandler
{
    public void handle(IHttpRequest request, IHttpResponse response);
}
