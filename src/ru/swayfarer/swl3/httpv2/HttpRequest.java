package ru.swayfarer.swl3.httpv2;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class HttpRequest implements IHttpRequest.IWritableHttpRequest
{
    @Internal
    @NonNull
    public HttpAddress target = new HttpAddress();

    @Internal
    @NonNull
    public HttpHeadersContainer headers = new HttpHeadersContainer();

    @Internal
    @NonNull
    public IFunction1NoR<DataOutStream> bodyWriter;

    @Override
    public HttpAddress target()
    {
        return target;
    }

    @Override
    public HttpHeadersContainer headers()
    {
        return headers;
    }

    @Override
    public IFunction1NoR<DataOutStream> bodyWriter()
    {
        return bodyWriter;
    }
}
