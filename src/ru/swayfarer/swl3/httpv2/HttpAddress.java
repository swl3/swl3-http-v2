package ru.swayfarer.swl3.httpv2;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class HttpAddress
{
    @Internal
    @NonNull
    public String method;

    @Internal
    @NonNull
    public String host;

    @Internal
    @NonNull
    public String uri;

    @Builder.Default
    @Internal
    @NonNull
    public int port = -1;

    @Builder.Default
    @Internal
    @NonNull
    public ExtendedMap<String, ExtendedList<String>> queryParams = new ExtendedMap<>();

    public HttpAddress addQueryParam(String name, String value)
    {
        queryParams.getOrCreate(name, ExtendedList::new).add(value);
        return this;
    }

    public ExtendedOptional<String> getQueryParam(String name)
    {
        return queryParams.getOptionalValue(name).map(ExtendedList::first);
    }
}
