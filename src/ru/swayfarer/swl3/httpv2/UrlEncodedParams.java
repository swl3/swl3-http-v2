package ru.swayfarer.swl3.httpv2;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.httpv2.IUrlEncodedParams.IWritableUrlEncodedParams;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringValue;
import ru.swayfarer.swl3.string.converters.StringConverters;

import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
public class UrlEncodedParams implements IWritableUrlEncodedParams
{
    @Internal
    @NonNull
    public ExtendedMap<String, ExtendedList<StringValue>> values = new ExtendedMap<>();

    @Internal
    @NonNull
    public StringConverters stringConverters;

    public UrlEncodedParams()
    {
        this(StringConverters.getInstance());
    }

    public UrlEncodedParams(@NonNull StringConverters stringConverters)
    {
        this.stringConverters = stringConverters;
    }

    @Override
    public ExtendedOptional<StringValue> getOptional(String paramName)
    {
        return ExtendedOptional.of(getCollection(paramName).first());
    }

    @Override
    public <T> ExtendedOptional<T> getOptional(String paramName, Class<T> type)
    {
        return ExtendedOptional.of(this.getCollection(paramName, type).first());
    }

    @Override
    public ExtendedList<StringValue> getCollection(String param)
    {
        return values.getOptionalValue(param).orElseGet(ExtendedList::new);
    }

    @Override
    public <T> ExtendedList<T> getCollection(String param, Class<T> classOfElement)
    {
        return values.getOptionalValue(param)
                .map((list) -> list.exStream()
                        .map((elem) -> elem.getValue(classOfElement))
                        .nonNull()
                        .toExList()
                )
                .orElseGet(ExtendedList::new);
    }

    @Override
    public Set<String> getKeys()
    {
        return values.keySet();
    }

    @Override
    public ExtendedOptional<IWritableUrlEncodedParams> asWritable()
    {
        return ExtendedOptional.of(this);
    }

    @Override
    public boolean isWritable()
    {
        return true;
    }

    @Override
    public void add(String name, Object value)
    {
        if (value != null)
        {
            values.getOrCreate(name, ExtendedList::new)
                    .add(
                        new StringValue(stringConverters, value::toString)
                    )
            ;
        }
    }

    @Override
    public void put(String name, Object value)
    {
        if (value != null)
        {
            values.getOrCreate(name, ExtendedList::new)
                    .setAll(
                            new StringValue(stringConverters, value::toString)
                    )
            ;
        }
    }
}
