package ru.swayfarer.swl3.httpv2;

import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.string.StringValue;

import java.util.Set;

public interface IUrlEncodedParams
{
    public ExtendedOptional<StringValue> getOptional(String paramName);
    public <T> ExtendedOptional<T> getOptional(String paramName, Class<T> type);

    public ExtendedList<StringValue> getCollection(String param);
    public <T> ExtendedList<T> getCollection(String param, Class<T> classOfElement);

    public Set<String> getKeys();

    public ExtendedOptional<IWritableUrlEncodedParams> asWritable();
    public boolean isWritable();


    public static interface IWritableUrlEncodedParams extends IUrlEncodedParams {
        public void add(String name, Object value);
        public void put(String name, Object value);
    }
}
