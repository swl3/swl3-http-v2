package ru.swayfarer.swl3.httpv2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class HttpResponse implements IHttpResponse.IWritableHttpResponse
{
    @NonNull
    @Internal
    public HttpCode code = new HttpCode(200);

    @NonNull
    @Internal
    public IFunction0<DataInStream> bodyInputStreamFun;

    @Internal
    public HttpHeadersContainer headers = new HttpHeadersContainer();

    @Internal
    @NonNull
    public String message = "";

    public HttpCode code()
    {
        return code;
    }

    @Override
    public String message()
    {
        return message == null ? "" : message;
    }

    public HttpHeadersContainer headers()
    {
        return headers;
    }

    public ExtendedOptional<DataInStream> body()
    {
        return ExtendedOptional.of(bodyInputStreamFun.apply());
    }

    public HttpResponse setBody(IFunction0<DataInStream> streamFun)
    {
        if (streamFun == null)
        {
            bodyInputStreamFun = () -> DataInStream.of(BytesInStream.of(new byte[0]));
            return this;
        }

        this.bodyInputStreamFun = streamFun;

        return this;
    }

    public HttpResponse setBody(DataInStream stream)
    {
        if (stream == null)
        {
            bodyInputStreamFun = () -> DataInStream.of(BytesInStream.of(new byte[0]));
            return this;
        }

        this.bodyInputStreamFun = () -> stream;
        return this;
    }

    @Override
    public HttpResponse setBody(byte[] bodyBytes)
    {
        return setBody(BytesInStream.of(bodyBytes));
    }
}
