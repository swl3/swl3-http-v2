package ru.swayfarer.swl3.httpv2;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.streams.DataOutStream;

public interface IHttpRequest
{
    public HttpAddress target();
    public HttpHeadersContainer headers();
    public IFunction1NoR<DataOutStream> bodyWriter();

    public default ExtendedOptional<IWritableHttpRequest> asWritable()
    {
        return ExtendedOptional.empty();
    }

    public static interface IWritableHttpRequest extends IHttpRequest{
        public IWritableHttpRequest setTarget(@NonNull HttpAddress address);
        public IWritableHttpRequest setHeaders(@NonNull HttpHeadersContainer headersContainer);
        public IWritableHttpRequest setBodyWriter(@NonNull IFunction1NoR<DataOutStream> writer);

        default ExtendedOptional<IWritableHttpRequest> asWritable()
        {
            return ExtendedOptional.of(this);
        }
    }
}
