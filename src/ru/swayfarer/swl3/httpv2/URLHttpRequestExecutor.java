package ru.swayfarer.swl3.httpv2;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.httpv2.IHttpResponse.IWritableHttpResponse;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class URLHttpRequestExecutor implements IHttpRequestExecutor
{
    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    @SneakyThrows
    public IWritableHttpResponse execute(IHttpRequest request)
    {
        var url = createUrl(request);
        var connection = url.openConnection();

        System.out.println(url);

        if (!(connection instanceof HttpURLConnection))
            ExceptionsUtils.throwException(new IllegalArgumentException("Not http request tried to execute: " + url));

        var httpConnection = (HttpURLConnection) connection;
        applyToUrlConnection(httpConnection, request);

        var code = httpConnection.getResponseCode();

        var response = new HttpResponse();
        response.setCode(new HttpCode(code));

        var is = (InputStream) null;

        var message = httpConnection.getResponseMessage();
        response.setMessage(message);

        if (code >= 400)
        {
            is = httpConnection.getErrorStream();
        }
        else
            is = httpConnection.getInputStream();

        var fis = is;
        response.setBodyInputStreamFun(() -> DataInStream.of(fis));

        for (var responseHeaders : httpConnection.getHeaderFields().entrySet())
        {
            response.getHeaders().setHeaders(responseHeaders.getKey(), CollectionsSWL.list(responseHeaders.getValue()));
        }

        return response;
    }

    @SneakyThrows
    public URL createUrl(IHttpRequest request)
    {
        var requestTarget = request.target();

        var buf = new DynamicString();

        buf.append(requestTarget.getHost());

        var port = requestTarget.getPort();
        if (port >= 0)
        {
            buf.append(":");
            buf.append(requestTarget.getPort());
        }

        buf.append(requestTarget.getUri());

        var queryParams = requestTarget.getQueryParams();

        if (!CollectionsSWL.isNullOrEmpty(queryParams))
        {
            var needsEnd = false;
            buf.append("?");
            for (var query : requestTarget.getQueryParams())
            {
                var key = query.getKey();
                var values = query.getValue();

                for (var value : values)
                {
                    if (needsEnd)
                        buf.append("&");

                    needsEnd = true;

                    buf.append(key);
                    buf.append("=");
                    buf.append(URLEncoder.encode(value, "UTF-8"));
                }
            }
        }

        return new URL(buf.toString());
    }

    @SneakyThrows
    public void applyToUrlConnection(HttpURLConnection urlConnection, IHttpRequest request)
    {
        if (request.headers().getFirstHeader("Cache-Control").orElse("").startsWith("no-cache"))
        {
            urlConnection.setUseCaches(false);
        }

        for (var rawHeader : request.headers().getRawHeaders())
        {
            for (var value : rawHeader.getValue())
            {
                urlConnection.setRequestProperty(rawHeader.getKey(), value);
            }
        }

        urlConnection.setRequestMethod(request.target().getMethod());

        var bodyWriter = request.bodyWriter();

        if (bodyWriter != null)
        {
            if (request.target().getMethod().equalsIgnoreCase("GET"))
                logger.warn("Request to", urlConnection.getURL(), "was marked by GET method, but has body! Switching to POST!");

            urlConnection.setDoOutput(true);
            urlConnection.connect();
            var os = urlConnection.getOutputStream();
            var dos = DataOutStream.of(os);
            bodyWriter.apply(dos);
            dos.flush();
        }
    }
}
